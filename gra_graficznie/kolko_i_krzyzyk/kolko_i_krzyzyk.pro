#-------------------------------------------------
#
# Project created by QtCreator 2017-05-23T08:58:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = kolko_i_krzyzyk
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    wezel.h \
    drzewo.h \
    gra.h \
    computer_game.h

FORMS    += mainwindow.ui
